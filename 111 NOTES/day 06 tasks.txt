DAY 06 TASKS AND HOMEWORK
=========================

Day06PeopleDualArray
--------------------

Declare two arrays:

String namesArray = new String[4];
int agesArray = new int[4];

Ask user for names and ages of four people and save the information in the arrays.

Find the youngest person and display their name and age.

Example session:
Enter name of person #1: Jerry
Enter age  of person #1: 33
Enter name of person #2: Tom
Enter age  of person #2: 22
Enter name of person #3: Marianna
Enter age  of person #3: 56
Enter name of person #4: Lucy
Enter age  of person #4: 27

Youngest person is 22 and their name is Tom

NOTE: Using Collections.sort() is neither needed nor useful to solve this task.


Day06PeopleFirst
----------------

Declare class Person with fields: String name, integer age.
Add a constructor to the class which takes two parameters name and age and assigns them to the fields.
Add method print() that will print out information about that Person, e.g.:

Jerry is 33 y/o

PART A

In the main class add the following field definition:

static ArrayList<Person> peopleList = new ArrayList<>();

In the main method of your project create 3 objects type Person using the constructor you've defined and add them to peopleList.
Then print out information about each of them using print() method on each of them in a for-each loop.

Find the youngest and oldest person and display their name and age.

NOTE: Using Collections.sort() is neither needed nor useful to solve this task.

PART B

In the main directory of the project create text file people.txt with content similar to the following:

Jerry;33
Maria;22
Theresa;54
Tom;45

Instead of instantiating 3 objects in the main method read in the contents of the file, parse it to create objects with provided names and ages.

Suggestion: use String.split() to separate data, then Integer.parseInt() to parse the age.

The print out each person's info on a separate line.

PART C

Make sure you're handling the necessary exceptions in case a line contains invalid information, such as:
- name empty
- age not within 0-150 range
- invalid number of semicolon-separated fields in line, e.g. J;;;56

In case of such violation inform the user, skip the line and continue parsing the next line of the input file.


Day06EnterPeople
----------------

Copy class Person from the previous project.

In your main method declare
ArrayList<Person> peopleList = new ArrayList<>();

In a loop keep asking user for name and age of person and keep adding them to the list until user enters an empty name.

Ask user for search string.

After user is done entering names/ages compute and display the following.
* Average age of all people (floating point with 2 decimal points precision)
* Youngest person's name and age
* Name and age of person with the shortest name
* Names and ages of all persons who's names contain the search string.

Example session:

Enter person's name (empty to finish): Jerry
Enter age: 33
Enter person's name (empty to finish): Mo
Enter age: 27
Enter person's name (empty to finish): Marianna
Enter age: 55
Enter person's name (empty to finish): Lillian
Enter age: 51
Enter person's name (empty to finish): 

Enter search string: n

Average age is: 41.50
Youngest person is: Mo 27 y/o
Person with shortest name is: Mo 27 y/o
Person with matching name: Marianna 55 y/o
Person with matching name: Lillian 51 y/o


Day06RandNums
-------------

Declare class RandomStore with the following methods:

int nextInt(minIncl, maxExcl) {}
void printHistory() {}

... and ArrayList field:

ArrayList<Integer> intHistory = new ArrayList<>();

This class does not need a constructor.

Method nextInt() generates a random integer value between minimum (inclusive) and maximum (exclusive), saves it in intHistory list and returns that value.
For the time being you can assume maximum is greater than minumum. You do not need to verify that.

Method printHistory() prints out all random values stored in intHistory array list comma-separated on a single line

Main Class

The main() method of your main class will contain test code. Example code.

public static void main(String[] args) {
	RandomStore rs = new RandomStore();
	int v1 = rs.nextInt(1, 10);
	int v2 = rs.nextInt(-100, -10);
	int v3 = rs.nextInt(-20,21);
	System.out.printf("Values %d, %d, %d\n", v1, v2, v3);
	rs.printHistory();
}

Example (possible) output:
Values 5, -57, -1
5, -57, -1

Quiz topics
-----------

* All the content of Programming I
* ArrayList, parsing integers and floating points
* basic exceptions use, try with resources to read/write text files
* basic class use (no getters/setters or verification, or throwing exceptions)



