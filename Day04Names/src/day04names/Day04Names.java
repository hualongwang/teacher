package day04names;

import java.util.Scanner;

public class Day04Names {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("How many names do you want to enter? ");
        int size = input.nextInt();
        input.nextLine(); // consume the left over newline
        String [] namesArray = new String[size];
        for (int i = 0; i < size; i++) {
            // System.out.print("Enter name " + (1+i) + "#: ");
            System.out.printf("Enter name %d#: ", i+1);
            String name = input.nextLine();
            namesArray[i] = name;
        }
        System.out.print("Your names were: ");
        // String listStr = String.join(", ", namesArray);
        // System.out.println(listStr);
        for (int i = 0; i < size; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", namesArray[i]);
        }
    }
    
}
