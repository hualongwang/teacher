package day05readwritefirst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day05ReadWriteFirst {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("Enter a line of text:");
        String userLine = input.nextLine();        
        
        try (PrintWriter fileOutput = new PrintWriter(new File("file.txt"))) {
            for (int i = 0; i < 3; i++) {
                fileOutput.println(userLine);
            }            
        } catch (IOException ex) {
            System.out.println("Error writing file: " + ex.getMessage());
        }
        //
        System.out.println("File contents:");
        try (Scanner fileInput = new Scanner(new File("file.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
        
    }
    
}
