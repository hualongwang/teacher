package day04numbers;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Day04Numbers {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> numsList = new ArrayList<>();
        System.out.print("How many random numbers do you want to generate? ");
        int count = input.nextInt();
        input.nextLine();
        for (int i = 0; i < count; i++) {
            int num = (int)(Math.random()*201 - 100);
            numsList.add(num);
        }
        //
        boolean hasFirstNumBeenDisplayed = false;
        for (int n : numsList) {
            if (n <= 0) {                
                System.out.printf("%s%d", hasFirstNumBeenDisplayed ? ", " : "", n);
                hasFirstNumBeenDisplayed = true;
            }
        }
        System.out.println("");
        
    }
    
}
