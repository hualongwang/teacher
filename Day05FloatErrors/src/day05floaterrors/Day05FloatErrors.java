package day05floaterrors;

public class Day05FloatErrors {
    public static void main(String[] args) {
        // this looop executes 11 times instead of 10
        for (double i = 0; i < 1; i += 0.1) {
            System.out.print("x ");
        }
        System.out.println("");
        /*
        double d1 = 0.76;
        double d2 = input.nextDouble();
        if (d1 == d2) {
            
        } */
        
        float value = 1.0f;
        for (int i = 0; i < 10; i++) {
            value = value - 0.1f;
        }
        System.out.println("Value is " + value);       
        System.out.printf("Value is %f", value);
    }
    
}
